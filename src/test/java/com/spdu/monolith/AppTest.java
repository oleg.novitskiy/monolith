package com.spdu.monolith;

import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

public class AppTest {

    @Test
    public void shouldReturn3UsersFromFile() throws URISyntaxException, IOException {
        List<String> expectedUsers = Arrays.asList("Alex", "Bob", "Bill");

        Path path = Paths.get(getClass().getClassLoader()
                .getResource("users.txt").toURI());

        List<String> actualUsers = Files.readAllLines(path);

        Assert.assertEquals(expectedUsers, actualUsers);
    }
}