package com.spdu.monolith.service;

import com.spdu.monolith.model.User;

import java.util.List;

public interface UserService {
    List<User> getUsersList();

    void add(User user);

    boolean remove(User user);
}
