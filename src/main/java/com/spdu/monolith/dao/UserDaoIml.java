package com.spdu.monolith.dao;

import com.spdu.monolith.model.User;

import java.util.ArrayList;
import java.util.List;

public class UserDaoIml implements UserDao {
    private List<User> users;

    public UserDaoIml() {
        users = new ArrayList<>();
    }

    public List<User> getUsersList() {
        return users;
    }

    public void add(User user) {
        users.add(user);
    }

    public boolean remove(User user) {
        return users.remove(user);
    }
}
