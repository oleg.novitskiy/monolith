package com.spdu.monolith;

import com.spdu.monolith.dao.UserDaoIml;
import com.spdu.monolith.model.User;
import com.spdu.monolith.service.UserService;
import com.spdu.monolith.service.UserServiceImpl;

public class App {

    public static void main(String[] args) {
        // Spring will do it later
        UserService userService = new UserServiceImpl(new UserDaoIml());
        final User alex = new User("Alex", 20);
        final User bill = new User("Bill", 30);
        final User tom = new User("Tom", 40);

        userService.add(alex);
        userService.add(bill);
        userService.add(tom);
        printUsers(userService);

        System.out.println("\nAfter remove user Alex.");
        userService.remove(alex);
        printUsers(userService);
    }


    private static void printUsers(UserService userService) {
        System.out.println("Users list:");
        userService.getUsersList().forEach(System.out::println);
    }
}
